﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Exercise_5
{
    public partial class Form1 : Form
    {
        public int modifier = 0;
        public Form1()
        {
            InitializeComponent();
            comboBox1.SelectedItem = "Dog";
            
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem.Equals("Dog"))
            {
                string s = comboBox2.SelectedItem.ToString();
                char c = char.Parse(s.Substring(s.Length - 1));
                modifier = c - '0';
                int pets = int.Parse(comboBox3.SelectedItem.ToString());
                modifier += pets;
            }
            else
            {
                string s = comboBox2.SelectedItem.ToString();
                char c = char.Parse(s.Substring(s.Length - 1));
                modifier = c - '0';
                modifier += 13;
                int pets = int.Parse(comboBox3.SelectedItem.ToString());
                modifier += pets;
            }
            Result res = new Result();
            res.label1.Text = modifier + "";
            res.ShowDialog();


        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem.Equals("Dog"))
            {
                comboBox2.Items.Clear();
                comboBox2.Items.Add("Hound");
                comboBox2.Items.Add("Retriever");
                comboBox2.Items.Add("Labrador");
                comboBox2.Items.Add("Rottweiler");
                comboBox2.Items.Add("Snoopy");
            }
            if (comboBox1.SelectedItem.Equals("Cat"))
            {
                comboBox2.Items.Clear();
                comboBox2.Items.Add("Scottish Fold");
                comboBox2.Items.Add("Cougar");
                comboBox2.Items.Add("Lion");
                comboBox2.Items.Add("Russian Blue");
                comboBox2.Items.Add("Persian Cat");
            }
        }
    }
}
